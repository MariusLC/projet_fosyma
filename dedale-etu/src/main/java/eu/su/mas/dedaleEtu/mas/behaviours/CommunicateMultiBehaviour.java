package eu.su.mas.dedaleEtu.mas.behaviours;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import dataStructures.serializableGraph.SerializableSimpleGraph;
import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import jade.core.AID;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import eu.su.mas.dedaleEtu.mas.agents.own.ExploreMultiAgent;

/**
 * This behavior is meant for agent to send their knowledge about the environement to an other agent
 * It also receives the knowledge from the other agent and merge it with theirs.
 * @author basile musquer, marius le chapelier
 *
 */

public class CommunicateMultiBehaviour extends SimpleBehaviour {

	private static final long serialVersionUID = 8567687976437927661L;
	private boolean finished = false;

	/**
	 * Current knowledge of the agent regarding the environment
	 */
	private MapRepresentation myMap;

	/**
	 * Names of the other agents
	 */
	private String receiver;
	
	/**
	 * List of closed nodes communicated to each agent.
	 */
	private Hashtable<String, Couple<List<String>, List<String>>> communicationMemory;
	
	public CommunicateMultiBehaviour(final AbstractDedaleAgent myagent, MapRepresentation data, String name, Hashtable<String, Couple<List<String>, List<String>>> communicationMemory) {
		super(myagent);
		this.myMap = data;
		this.receiver = name;
		if (communicationMemory != null) {
			this.communicationMemory = communicationMemory;
		} else {
			this.communicationMemory = communicationMemory;
			System.out.println("\n!!!!!!!! communicationMemory is null");
		}
		
	}

	@Override
	public void action() {
		Couple<List<String>, List<String>> nodes = this.myMap.getNodes();
		
		//1°Create the message
        final ACLMessage msgToSend = new ACLMessage(ACLMessage.INFORM);
        msgToSend.setSender(this.myAgent.getAID());
		msgToSend.setProtocol("ShareMapProtocol");
        msgToSend.addReceiver(new AID(receiver, AID.ISLOCALNAME));
            
        //2° send the map
        try {
            msgToSend.setContentObject(this.myMap.getSerialiazableData(this.communicationMemory.get(this.receiver), nodes));
        } catch (IOException e) {
            System.out.println("Unserializable object");
            e.printStackTrace();
        }
        this.myAgent.send(msgToSend);
        
        //3° receive the map from the other agent
        final MessageTemplate msgTemplate = MessageTemplate.and(
                MessageTemplate.MatchPerformative(ACLMessage.INFORM),
                MessageTemplate.and(MessageTemplate.MatchSender(new AID(this.receiver, AID.ISLOCALNAME)),
                MessageTemplate.MatchProtocol("ShareMapProtocol")));
        final ACLMessage inmsg = this.myAgent.blockingReceive(msgTemplate, 500); // Wait 0.1 s
        if (inmsg != null) {
        	try {
	            // If message received, merge your infos
	            System.out.println(this.myAgent.getLocalName()+"<----Result received from "+inmsg.getSender().getLocalName()+" in ComMultiBehaviour");
	            this.myMap.merge((SerializableSimpleGraph<String, MapAttribute>) inmsg.getContentObject());
	            //set Memory
	            this.communicationMemory.remove(this.receiver);
	            this.communicationMemory.put(this.receiver, nodes);
	            // set the timer
	            ((ExploreMultiAgent) myAgent).resetTimer(this.receiver);
        	} catch (UnreadableException e) {
        		e.printStackTrace();
        	}
        }

		
		// On vire tous les messages du receiver
		ACLMessage msg;
		do {
			msg = this.myAgent.receive(MessageTemplate.and(
	                MessageTemplate.MatchPerformative(ACLMessage.INFORM),
	                MessageTemplate.MatchSender(new AID(this.receiver, AID.ISLOCALNAME))));
		} while(msg != null);
		//
		
		System.out.println(this.myAgent.getLocalName() + " finished communicate");
		this.myAgent.addBehaviour(new ExploMultiBehaviour((AbstractDedaleAgent) this.myAgent, this.myMap, this.communicationMemory));
		this.finished = true;
		this.done();
    }

	@Override
	public boolean done() {
		return finished;
	}
}